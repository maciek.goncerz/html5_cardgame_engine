# html5_cardgame_engine

HCG Engine is basic 2D game engine written with HTML5/CSS/JS/Python.

Third-party:
* JQuery    https://jquery.com/
* Crypto-JS https://github.com/brix/crypto-js
* JS Cookie https://github.com/js-cookie/js-cookie