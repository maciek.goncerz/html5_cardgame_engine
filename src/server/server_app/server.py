import os
import json
import shutil

from .player import Player
from .game_app import Game

"""

    General functions:
        - loadGames

        - login
        - addNewPlayer
        - connectPlayer
        - disconnectPlayer

    Server-related functions:
        - 
        
"""

__DEBUG__ = True

class CatanServer:
    def __init__(self):
        self.players = {}
        self.games = {}

        self._loadGames()

    def _loadGames(self):
        ids = os.listdir('games')

        for id in ids:
            self.games[id] = Game(server=self, ID=id)

    # basic user-related functions
    async def login(self, ws, name, hash):
        if __DEBUG__:
            print('Logging {} {}'.format(name, hash))

        return self.players[ws].login(name, hash)
    async def addNewPlayer(self, name, hash):
        if __DEBUG__:
            print('Adding new player {} {}'.format(name, hash))

        return Player.addNew(name, hash)
    async def connectPlayer(self, ws):
        if __DEBUG__:
            print('Connected new player')

        self.players[ws] = Player()
    async def disconnectPlayer(self, ws):
        if __DEBUG__:
            print('Disconnected {}'.format(self.players[ws].name))

        del self.players[ws]

    # game management-related functions
    async def newGame(self, ws, action_object):
        new_game = Game(self)
        while new_game.id in self.games:
            new_game = Game(self)

        new_game.setAdmin(self.players[ws].name)
        new_game.joinLobby(self.players[ws].name)
        new_game.save()
        
        self.games[new_game.id] = new_game

        self.players[ws].joinGame(new_game.id)
        await self.updatePlayers(new_game.id)  
        await self.updateUserStatus(ws)

    async def joinGame(self, ws, action_object):
        if not self.players[ws].logged:
            return
        
        if 'game' not in action_object:
            return

        if action_object['game'] in self.players[ws].joined_games:
            return

        if action_object['game'] not in self.games:
            return

        if self.games[action_object['game']].joinLobby(self.players[ws].name) == True:
            self.players[ws].joinGame(action_object['game'])
            await self.updateUserStatus(ws)
            return True
        else:
            return False

    async def removeGame(self, ws, action_object):
        game_id = action_object['game']
        if game_id not in self.games:
            return

        if self.games[game_id].admin != self.players[ws].name:
            return

        # remove / move game directory
        shutil.rmtree('games/{}'.format(game_id), ignore_errors=True)

        # remove game id from self.games
        del self.games[game_id]

        # remove game from players info -? efficient way
        Player.removeGame(game_id)
        for player in self.players:
            self.players[player].loadJoinedGames()
            await self.updateUserStatus(player)

    # action-related functions
    async def action(self, ws, action_object):
        if not self.players[ws].logged:
            return

        if 'new_game' in action_object:
            await self.newGame(ws, action_object)
            return

        if action_object['game'] not in self.players[ws].joined_games:
            return

        await self.games[action_object['game']].performAction(self.players[ws].name, action_object)

    # response functions
    # update single player (full info)
    async def updatePlayer(self, ws, game_id):
        if game_id not in self.players[ws].joined_games:
            return

        await self.updatePlayerStatus(ws, json.dumps({
            'game_status': {
                'player': self.games[game_id].getPlayerStatus(self.players[ws].name),
                'game': self.games[game_id].getGameStatus()
            }
        }))

    # update every player in this game (full info)
    async def updatePlayers(self, game_id):
        print('Updating players for game #{}'.format(game_id))

        game_status = self.games[game_id].getGameStatus()

        for ws in self.players:
            if game_id in self.players[ws].joined_games:
                print('Sending player\' status {}'.format(self.players[ws].name))
                await self.updatePlayerStatus(ws, json.dumps({
                    'game_status': {
                        'player': self.games[game_id].getPlayerStatus(self.players[ws].name),
                        'game': game_status
                    }
                }))

    # next stage of updatePlayers
    async def updatePlayerStatus(self, ws, game_status):
        await ws.send('S;{}'.format(game_status))

    async def updateUserStatus(self, ws):
        await ws.send('S;{}'.format(json.dumps({
            'user_status': self.getUserStatus(self.players[ws])
        })))

    def getUserStatus(self, player):
        return {
            'name': player.name,
            'games': player.joined_games
        }