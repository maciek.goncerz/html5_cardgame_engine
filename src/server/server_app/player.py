import json

class Player:
    def __init__(self):
        self.name = 'nieznany'
        self.logged = False
        self.joined_games = []

    def login(self, name, hash):
        # player needs to reconnect to relog
        if self.logged:
            return

        # acquire current login credentials
        with open('logins.json', 'r') as fs:
            logins = json.load(fs)

        # check if given name&hash matches any
        if name in logins:
            if hash == logins[name]:
                # set proper variables
                self.name = name
                self.logged = True
                self.loadJoinedGames()
                return True

        return False

    def loadJoinedGames(self):
        with open('players.json', 'r') as fs:
            data = json.load(fs)
            if self.name not in data:
                self.updateJoinedGames()
                return
            self.joined_games = data[self.name]['games']

    def updateJoinedGames(self):
        data = None
        with open('players.json', 'r') as fs:
            data = json.load(fs)

        if self.name not in data:
            data[self.name] = {
                'games': self.joined_games
            }
        else:
            data[self.name]['games'] = self.joined_games

        with open('players.json', 'w') as fs:
            json.dump(data, fs)

    def joinGame(self, game_id):
        if game_id in self.joined_games:
            return

        self.joined_games.append(game_id)
        self.updateJoinedGames()

    @staticmethod
    def addNew(name, hash):
        # acquire current login credentials
        with open('logins.json', 'r') as fs:
            logins = json.load(fs)

        # stop if player is already in
        if name in logins:
            return False

        logins[name] = hash

        print(logins)

        # save new login credentials
        with open('logins.json', 'w') as fs:
            json.dump(logins, fs)

        return True

    @staticmethod
    def removeGame(game_id):
        data = None
        with open('players.json', 'r') as fs:
            data = json.load(fs)

        for player in data:
            if game_id in data[player]['games']:
                data[player]['games'].remove(game_id)

        with open('players.json', 'w') as fs:
            json.dump(data, fs)
