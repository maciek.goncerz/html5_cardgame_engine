from .map_road import MapRoad

"""
Borders:
     1    2
       /\    
    6 |  | 3
       \/
     5    4  

Tiles:
     0   2   3   4

0:   1   2   3   4
1:     5   6   7   8
2:   9   10  11  12
3:     13  14  15  16

"""

class MapSettlement:

    @classmethod
    def canBeAdded(cls, map, tile, border, color):
        if border <= 0 or border > 6:
            return False
        
        if color <= 0 or color > 8:
            return False

        if cls.isOccupied(map, tile, border):
            return False

        # check if road in this color is adjacent to this position
        adjacent_roads = cls.getSiblings(map, tile, border)
        adjacent_roads.append(tile, border)

        for road in adjacent_roads:
            c = MapRoad.getRoad(map, road[0], road[1])
            if color == c:
                return True

        # TODO: check if no settlement is in any adjacent corner

        return False
        
    @classmethod
    def addSettlement(cls, map, tile, border, color):
        map.settlements.append({
            'x': tile[0],
            'y': tile[1],
            'border': border,
            'color': color
        })

    @classmethod
    def getSiblings(cls, map, tile, border):
        if border == 1:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] - 1), 3),
                    ((tile[0], tile[1] - 1), 5)
                ]
            else:
                return [
                    ((tile[0], tile[1] - 1), 3),
                    ((tile[0] + 1, tile[1] - 1), 5)
                ]

        elif border == 2:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0], tile[1] - 1), 4),
                    ((tile[0] + 1, tile[1]), 6)
                ]
            else:
                return [
                    ((tile[0] + 1, tile[1] - 1), 4),
                    ((tile[0] + 1, tile[1]), 6)
                ]

        elif border == 3:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] + 1, tile[1]), 5),
                    ((tile[0], tile[1] + 1), 1)
                ]
            else:
                return [
                    ((tile[0] + 1, tile[1]), 5),
                    ((tile[0] + 1, tile[1] + 1), 1)
                ]

        elif border == 4:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] + 1), 6),
                    ((tile[0], tile[1] + 1), 3)
                ]
            else:
                return [
                    ((tile[0], tile[1] + 1), 6),
                    ((tile[0] + 1, tile[1] + 1), 3)
                ]

        elif border == 5:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1]), 3),
                    ((tile[0] - 1, tile[1] + 1), 1)
                ]
            else:
                return [
                    ((tile[0] - 1, tile[1]), 3),
                    ((tile[0], tile[1] + 1), 1)
                ]

        elif border == 6:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] + 1), 4),
                    ((tile[0] - 1, tile[1]), 2)
                ]
            else:
                return [
                    ((tile[0], tile[1] + 1), 4),
                    ((tile[0] - 1, tile[1]), 2)
                ]

    # returns entire city dictionary or None
    @classmethod
    def getSettlement(cls, map, tile, border):
        siblings = cls.getSiblings(map, tile, border)

        for settlement in map.settlements:
            if tile[0] == settlement['x'] and tile[1] == settlement['y'] and border == settlement['border']:
                return settlement

            if siblings[0][0][0] == settlement['x'] and tile[0][0][1] == settlement['y'] and siblings[0][1] == settlement['border']:
                return settlement

            if siblings[1][0][0] == settlement['x'] and siblings[1][0][1] == settlement['y'] and siblings[1][1] == settlement['border']:
                return settlement

        return None

    @classmethod
    def isOccupied(cls, map, tile, border):
        settlement = cls.getSettlement(map, tile, border)

        if settlement is None:
            return False
        return True