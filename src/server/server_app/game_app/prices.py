class Prices:
    class Entity:
        @classmethod
        def isEnough(cls, resources):
            return resources['Wood'] >= cls.Wood and
                resources['Clay'] >= cls.Clay and
                resources['Sheep'] >= cls.Sheep and
                resources['Crops'] >= cls.Crops and
                resources['Rocks'] >= cls.Rocks

        @classmethod
        def subtract(cls, resources):
            resources['Wood'] -= cls.Wood
            resources['Clay'] -= cls.Clay
            resources['Sheep'] -= cls.Sheep
            resources['Crops'] -= cls.Crops
            resources['Rocks'] -= cls.Rocks

    class Village(Prices.Entity):
        Wood = 1
        Clay = 1
        Sheep = 1
        Crops = 1
        Rocks = 0

    class City(Prices.Entity):
        Wood = 0
        Clay = 0
        Sheep = 0
        Crops = 2
        Rocks = 3

    class Path(Prices.Entity):
        Wood = 1
        Clay = 1
        Sheep = 0
        Crops = 0
        Rocks = 0

    class Development(Prices.Entity):
        Wood = 0
        Clay = 0
        Sheep = 1
        Crops = 1
        Rocks = 1