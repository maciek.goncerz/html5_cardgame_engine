import json
import random
import time
import os

from .map import Map

class DC:
    KnightCard = 100
    ProgressCard = 101
    VictoryPointCard = 102

class Resource:
    Brick = 'b'
    Lumber = 'l'
    Ore = 'o'
    Grain = 'g'
    Wool = 'w'

class Price:
    Devepment = {
        Resource.Ore: 1,
        Resource.Grain: 1,
        Resource.Wool: 1
    }
    Settlement = {
        Resource.Brick: 1,
        Resource.Grain: 1,
        Resource.Wool: 1,
        Resource.Lumber: 1
    }
    City = {
        Resource.Grain: 2,
        Resource.Ore: 3
    }
    Road = {
        Resource.Lumber: 1,
        Resource.Brick: 1
    }

class Color:
    Red = 1
    Green = 2
    Blue = 3
    Black = 4
    Orange = 5
    Purple = 6
    White = 7
    Yellow = 8

class GameStage:
    Lobby = 0
    """
        In this stage players:
        - choose their color
        - get familiar with game settings
        - choose direction of turns
    """
    
    InGame = 1
    """
        This stage starts with new turn for Red(1) player.
    """

    Finished = 2
    """
        Game in this stage can be accessed by anyone.
        It displays statistics.
    """

class Game:
    @staticmethod
    def generateID():
        chain = []
        for x in range(11):
            chain.append(random.randint(0, 9))
        
        return '{}{}{}-{}{}{}{}-{}{}{}{}'.format(
            chain[0], chain[1], chain[2],
            chain[3], chain[4], chain[5], chain[6],
            chain[7], chain[8], chain[9], chain[10]
        )

    def createGameDirectory(self):
        os.mkdir('games/{}'.format(self.id))

    def __init__(self, server, ID=None):
        if ID is None:
            self.id = Game.generateID()
            self.map = Map(self)
            self.createGameDirectory()
        else:
            self.id = ID
            self.map = None

        # time
        self.time_lobby_created = time.time()
        self.time_game_started = None
        self.time_game_finished = None

        # general variables
        self.server_instance = server
        self.stage = GameStage.Lobby
        self.admin = ''
        self.players = {}   # player name -> color code
        self.colors = {}    # color code -> player name

        # Lobby-related variables
        self.lobby_stage = 0
        self.lobby_accepted = {}
        self.max_players = 4

        self.presettings = {}
        self.lobby_turn = 0

        # Ingame-related variables
        self.current_turn = 0
        self.current_player = 0
        self.players_queue = []

        self.special_cards = {
            'longest_road': '',
            'largest_army': ''
        }

        self.resources = {}
        self.knights_count = {}

        self.points = {}
        self.development_order = []
        self.development_cards = {
            'knight_01': '',
            'knight_02': '',
            'knight_03': '',
            'knight_04': '',
            'knight_05': '',
            'knight_06': '',
            'knight_07': '',
            'knight_08': '',
            'knight_09': '',
            'knight_10': '',
            'knight_11': '',
            'knight_12': '',
            'knight_13': '',
            'knight_14': '',
            'victory_point_1': '',
            'victory_point_2': '',
            'victory_point_3': '',
            'victory_point_4': '',
            'victory_point_5': '',
            'progress_monopoly_1': '',        # Monopoly (1 type of resource from every player)
            'progress_monopoly_2': '',        # Monopoly (1 type of resource from every player)
            'progress_plenty_1': '',          # Year of plenty (2 resources from the bank)
            'progress_plenty_2': '',          # Year of plenty (2 resources from the bank)
            'progress_roads_1': '',           # Road builing (build 2 roads)
            'progress_roads_2': ''}           # Road builing (build 2 roads)

        self.current_roll = [0, 0]

        if ID is not None:
            self.load()

    # admin tools
    def setAdmin(self, player):
        self.admin = player

    # lobby-related
    def selectColor(self, player, color):
        if color < 1 or color > 8:
            return

        for p in self.players:
            if color == p:
                return

        self.players[player] = color

        self.save()
    def deselectColor(self, player):
        self.players[player] = 0

        self.save()
    def acceptGame(self, player):
        self.lobby_accepted[player] = True

        self.save()
    def rejectGame(self, player):
        self.lobby_accepted[player] = False

        self.save()

    def _isPlayersLobbyTurn(self, player):
        if len(self.players_queue) == 0:
            return False

        if self.lobby_turn >= len(self.players_queue):
            return False

        if self.players_queue[self.lobby_turn] == player:
            return True
        else:
            return False
    def chooseSettlement(self, player, position):
        if not self._isPlayersLobbyTurn(player):
            return

        if self.presettings[player]['s1'] is None:
            self.presettings[player]['s1'] = position

            self.save()

            return

        if self.presettings[player]['s2'] is None:
            self.presettings[player]['s2'] = position

            self.save()

            return
    def chooseRoad(self, player, position):
        if not self._isPlayersLobbyTurn(player):
            return

        if self.presettings[player]['r1'] is None:
            self.presettings[player]['r1'] = position

            self.save()

            return

        if self.presettings[player]['r2'] is None:
            self.presettings[player]['r2'] = position

            self.save()

            return
    def resetPresettings(self, player):
        if not self._isPlayersLobbyTurn(player):
            return

        self.presettings[player] = {
            's1': None, 
            's2': None, 
            'r1': None, 
            'r2': None
        }

        self.save()

    def acceptPresettings(self, player):
        if not self._isPlayersLobbyTurn(player):
            return

        if self.presettings[player]['s1'] is None:
            return
        if self.presettings[player]['s2'] is None:
            return
        if self.presettings[player]['r1'] is None:
            return
        if self.presettings[player]['r2'] is None:
            return

        self.lobby_turn += 1

        self.save()
    def acceptPlayersInLobby(self, player):
        if player != self.admin:
            return

        self.lobby_stage = 1

        # shuffle players queue
        self.players_queue = list(self.players.keys())
        random.shuffle(self.players_queue)

        self.save()
    def reshuffleMap(self, player):
        if player != self.admin:
            return

        if self.lobby_stage > 0 or self.stage > GameStage.Lobby:
            return

        self.map = Map(self)

        self.save()

    def joinLobby(self, player):
        if self.stage != GameStage.Lobby or self.lobby_stage == 1:
            return False

        if len(self.players) >= self.max_players:
            return False

        self.players[player] = 0
        self.lobby_accepted[player] = False
        self.presettings[player] = {'s1': None, 's2': None, 'r1': None, 'r2': None}

        self.save()

        return True
    def leaveLobby(self, player):
        # player cannot leave if game has started
        if self.stage != GameStage.Lobby or self.lobby_stage == 1:
            return False

        del self.players[player]
        del self.lobby_accepted[player]
        del self.presettings[player]

        self.save()

        return True
    def startGame(self, player):
        if player != self.admin:
            return

        # change admins acceptance to True as he obviously wants to start the game
        self.lobby_accepted[player] = True

        for p in self.lobby_accepted:
            if p == False:
                return

        # check if presettings turn has finished
        if self.lobby_turn < len(self.players_queue):
            return

        self.stage = GameStage.InGame
        self.time_game_started = time.time()
        self.initGame()
        self.newTurn()
    def initGame(self):
        # set all resources to 0
        for p in self.players:
            self.resources[p] = {
                Resource.Brick: 0,
                Resource.Lumber: 0,
                Resource.Ore: 0,
                Resource.Grain: 0,
                Resource.Wool: 0
            }
            self.points[p] = 0
            self.knights_count[p] = 0
            self.colors[self.players[p]] = p

        # reverse lobby players queue
        self.players_queue.reverse()

        # shuffle development cards
        self.development_order = self.development_cards.keys()
        random.shuffle(self.development_order)

        # initialize new map
        self.map = Map(self)

        # TODO: add presettings to the map

    # ingame-related
    def hasWon(self, player):
        if self.points[player] >= 10:
            return True
        return False

    def finishGame(self, winner):
        self.time_game_finished = time.time()
        pass

    def newTurn(self):
        # get current player
        self.current_turn += 1
        self.current_player = self.players_queue[self.current_turn % len(self.players_queue)]

        # check if he has won
        if self.hasWon(self.current_player):
            self.finishGame(self.current_player)

        # start new turn with rolling 2 dices
        self.current_roll = [
            random.randint(1, 6),
            random.randint(1, 6)
        ]

        # save game for restore action
        self.save()

    def finishTurn(self):
        self.recalculatePoints()
        self.newTurn()

    def addPlayer(self, name, color):
        pass

    def recalculatePoints(self):
        # Points are granted by:
        # - number of villages
        # - number of cities
        # - number of Victory Point Cards
        # - special cards (longest path, knights' power)

        for p in self.players:
            self.points[p] = 0

        for vp in [1, 2, 3, 4, 5, 6]:
            if self.development_cards['victory_point_{}'.format(vp)] != '':
                self.points[self.development_cards['victory_point_{}'.format(vp)]] += 1

        for settlement in self.map.settlements:
            self.points[self.colors[settlement['color']]] += 1
            
        for city in self.map.cities:
            self.points[self.colors[city['color']]] += 2
        
        if self.special_cards['longest_road'] != '':
            self.points[self.special_cards['longest_road']] += 2
            
        if self.special_cards['largest_army'] != '':
            self.points[self.special_cards['largest_army']] += 2

    async def performAction(self, player, action_object):
        if self.stage == GameStage.Lobby:
            if player not in self.players:
                return

            if action_object['action'] == 'choose_color':
                self.selectColor(player, action_object['color'])

            elif action_object['action'] == 'deselect_color':
                self.deselectColor(player)

            elif action_object['action'] == 'accept':
                self.acceptGame(player)

            elif action_object['action'] == 'reject':
                self.rejectGame(player)

            elif action_object['action'] == 'accept_players':
                self.acceptPlayersInLobby(player)

            elif action_object['action'] == 'start':
                self.startGame(player)

            elif action_object['action'] == 'kick':
                pass

            elif action_object['action'] == 'settlement':
                if 'position' not in action_object:
                    return
                if 'x' not in action_object['position']:
                    return
                if 'y' not in action_object['position']:
                    return
                if 'border' not in action_object['position']:
                    return

                self.chooseSettlement(player, action_object['position'])

            elif action_object['action'] == 'road':
                if 'position' not in action_object:
                    return
                if 'x' not in action_object['position']:
                    return
                if 'y' not in action_object['position']:
                    return
                if 'border' not in action_object['position']:
                    return

                self.chooseRoad(player, action_object['position'])

            elif action_object['action'] == 'clear_presettings':
                self.resetPresettings(player)

            await self.server_instance.updatePlayers(self.id)

        elif self.stage == GameStage.InGame:
            if player != self.current_player:
                return
            
            if   action_object['action'] == 'build_road':
                self.buyRoad(player, action_object['tile'], action_object['border'])

            elif action_object['action'] == 'build_settlement':
                self.buySettlement(player, action_object['tile'], action_object['border'])

            elif action_object['action'] == 'build_city':
                self.buyCity(player, action_object['tile'], action_object['border'])

            elif action_object['action'] == 'buy_development':
                self.buyDevelopment(player)

            elif action_object['action'] == 'use_card':
                self.useCard(player, action_object['card'], action_object['args'])

            elif action_object['action'] == 'restore':
                self.load(full=True)

            elif action_object['action'] == 'finish_turn':
                self.finishTurn()
                
            await self.server_instance.updatePlayers(self.id)

        elif self.stage == GameStage.Finished:
            pass

    def canAffort(self, player, price):
        for resource in price:
            if self.resources[player][resource] < price[resource]:
                return False
        return True

    def reduceResources(self, player, price):
        for resource in price:
            self.resource[player][resource] -= price[resource]

    def buyDevelopment(self, player):
        if len(self.development_order) == 0:
            return

        if not self.canAffort(player, Price.Devepment):
            return

        bought = self.development_order[0]
        self.development_order.remove(bought)
        self.development_cards[bought] = player

        if 'victory' in bought:
            if self.hasWon(player):
                self.finishGame(player)

    def buyRoad(self, player, tile, border):
        if not self.canAffort(player, Price.Road):
            return

        if not self.map.addRoad(tile, border, self.players[player]):
            return

        self.reduceResources(player, Price.Road)
        self.calculateLongestRoad()

    def buySettlement(self, player, tile, border):
        if not self.canAffort(player, Price.Settlement):
            return

        if not self.map.addSettlement(tile, border, self.players[player]):
            return

        self.reduceResources(player, Price.Settlement)
        self.calculateLongestRoad()

    def buyCity(self, player, tile, border):
        if not self.canAffort(player, Price.City):
            return
        
        if not self.map.addCity(tile, border, self.players[player]):
            return

        self.reduceResources(player, Price.Settlement)
        self.calculateLongestRoad()

    def getResourceFromEverybody(self, resource):
        resource_count = 0

        for player in self.players:
            resource_count += self.resources[player][resource]
            self.resources[player][resource] = 0

        return resource_count

    def useCard(self, player, card, args):
        # args['new_pos'] - new robber position
        if 'knight' in card:
            for card_num in range(1, 14):
                card_name = 'knight_{:02d}'.format(card_num)

                if self.development_cards[card_name] != player:
                    continue

                if self.map.setRobber(args['new_pos'], self.players[player]):
                    self.knights_count[player] += 1
                    self.development_cards[card_name] = '-'
                    break

        # not sure if it's neccesary
        elif 'victory' in card:
            pass

        # args['res'] - resource to acquire
        elif 'monopoly' in card:
            for card_name in ['progress_monopoly_1', 'progress_monopoly_2']:
                if self.development_cards[card_name] != player:
                    continue

                resource_count = self.getResourceFromEverybody(args['res'])
                self.resources[player][args['res']] = resource_count

                self.development_cards[card_name] = '-'
                break

        # args['res1'] - 1st resource
        # args['res2'] - 2nd resource
        elif 'plenty' in card:
            for card_name in ['progress_plenty_1', 'progress_plenty_2']:
                if self.development_cards[card_name] != player:
                    continue

                self.resources[player][args['res1']] += 1
                self.resources[player][args['res2']] += 1
                
                self.development_cards[card_name] = '-'
                break

        # args['tile1']
        # args['border1']
        # args['tile2']
        # args['border2']
        elif 'road' in card:
            for card_name in ['progress_plenty_1', 'progress_plenty_2']:
                if self.development_cards[card_name] != player:
                    continue

                if not self.map.isNewRoadValid(args['tile1'], args['border1'], self.players[player]):
                    return

                if not self.map.isNewRoadValid(args['tile2'], args['border2'], self.players[player]):
                    return

                self.map.addRoad(args['tile1'], args['border1'], self.players[player])
                self.map.addRoad(args['tile2'], args['border2'], self.players[player])

                self.calculateLongestRoad()

        self.recalculatePoints()
        if self.hasWon(player):
            self.finishGame(player)

    def calculateLongestRoad(self):
        # TODO: check rules book as it's more complicated

        # TODO: verify & change longest road card owner if neccesary
        pass

    """
        Player specific information:
            - calculated victory points
            - current longest road (?)
            - available resources
            - available development cards
            - knights count
            - game ID
            - game stage
    """
    def getPlayerStatus(self, player):
        dev_cards = []
        for dc in self.development_cards:
            if self.development_cards[dc] == player:
                dev_cards.append(dc)

        if self.stage == GameStage.Lobby:
            return {
                'id': self.id,
                'stage': self.stage
            }

        else:
            return {
                'points': self.points[player],
                'longest_road': 0, # TODO: calculate
                'resources': self.resources[player],
                'knights_count': self.knights_count[player],
                'development_cards': dev_cards,
                'id': self.id,
                'stage': self.stage
            }

    """
        Player non-specific information:
            - current map status
            - game queue
            - game admin
            - players list with colours
            - current player
            - special cards owners
            - current dice roll
            - game ID
            - game stage
            - lobby accepted (if needed)
    """
    def getGameStatus(self):
        if self.stage == GameStage.Lobby:
            return {
                'admin': self.admin,
                'players': self.players,
                'lobby': self.lobby_accepted,
                'turn': self.lobby_turn,
                'max_players': self.max_players,
                'lobby_stage': self.lobby_stage,
                'presettings': self.presettings,
                'queue': self.players_queue,
                'map': self.map.toSerializable(),
                'id': self.id,
                'stage': self.stage
            }

        else:
            return {
                'map': self.map.toSerializable(),
                'admin': self.admin,
                'players': self.players,
                'queue': self.players_queue,
                'current': self.current_player,
                'roll': self.current_roll,
                'special_cards': self.special_cards,
                'lobby': self.lobby_accepted,
                'id': self.id,
                'stage': self.stage
            }

    def save(self, clean=False):
        save_dict = {
            'stage': self.stage,
            'admin': self.admin,
            'players': self.players,
            'colors': self.colors,

            'lobby_stage': self.lobby_stage,
            'lobby_accepted': self.lobby_accepted,
            'max_players': self.max_players,
            'presettings': self.presettings,
            'lobby_turn': self.lobby_turn,

            'time_lobby_created': self.time_lobby_created,
            'time_game_started': self.time_game_started,
            'time_game_finished': self.time_game_finished,

            'current_turn': self.current_turn,
            'current_player': self.current_player,
            'players_queue': self.players_queue,

            'special_cards': self.special_cards,

            'resources': self.resources,
            'knights_count': self.knights_count,
            
            'points': self.points,
            'development_order': self.development_order,
            'development_cards': self.development_cards,

            'current_roll': self.current_roll
        }

        with open('games/{}/status.json'.format(self.id), 'w') as fs:
            json.dump(save_dict, fs)

        with open('games/{}/map.json'.format(self.id), 'w') as fs:
            fs.write(self.map.toJSON())

    def load(self):
        with open('games/{}/status.json'.format(self.id), 'r') as fs:
            load_dict = json.load(fs)

            self.stage = load_dict['stage']
            self.admin = load_dict['admin']
            self.players = load_dict['players']
            self.colors = load_dict['colors']

            self.lobby_accepted = load_dict['lobby_accepted']
            self.lobby_stage = load_dict['lobby_stage']
            self.max_players = load_dict['max_players']
            self.presettings = load_dict['presettings']
            self.lobby_turn = load_dict['lobby_turn']

            self.time_lobby_created = load_dict['time_lobby_created']
            self.time_game_started = load_dict['time_game_started']
            self.time_game_finished = load_dict['time_game_finished']

            self.current_turn = load_dict['current_turn']
            self.current_player = load_dict['current_player']
            self.players_queue = load_dict['players_queue']

            self.special_cards = load_dict['special_cards']

            self.resources = load_dict['resources']
            self.knights_count = load_dict['knights_count']

            self.points = load_dict['points']
            self.development_order = load_dict['development_order']
            self.development_cards = load_dict['development_cards']

            self.current_roll = load_dict['current_roll']

        with open('games/{}/map.json'.format(self.id), 'r') as fs:
            loaded_map = fs.read()

            self.map = Map(self)
            self.map.fromJSON(loaded_map)
