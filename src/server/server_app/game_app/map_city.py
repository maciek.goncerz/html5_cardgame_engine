from .map_settlement import MapSettlement

"""
Borders:
     1    2
       /\    
    6 |  | 3
       \/
     5    4  

Tiles:
     0   2   3   4

0:   1   2   3   4
1:     5   6   7   8
2:   9   10  11  12
3:     13  14  15  16

"""

class MapCity:

    @classmethod
    def canBeAdded(cls, map, tile, border, color):
        if border <= 0 or border > 6:
            return False
        
        if color <= 0 or color > 8:
            return False

        existing_village = MapVillage.getVillage(map, tile, border)

        if existing_village is None:
            return False

        return True        

    @classmethod
    def getSiblings(cls, map, tile, border):
        if border == 1:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] - 1), 3),
                    ((tile[0], tile[1] - 1), 5)
                ]
            else:
                return [
                    ((tile[0], tile[1] - 1), 3),
                    ((tile[0] + 1, tile[1] - 1), 5)
                ]

        elif border == 2:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0], tile[1] - 1), 4),
                    ((tile[0] + 1, tile[1]), 6)
                ]
            else:
                return [
                    ((tile[0] + 1, tile[1] - 1), 4),
                    ((tile[0] + 1, tile[1]), 6)
                ]

        elif border == 3:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] + 1, tile[1]), 5),
                    ((tile[0], tile[1] + 1), 1)
                ]
            else:
                return [
                    ((tile[0] + 1, tile[1]), 5),
                    ((tile[0] + 1, tile[1] + 1), 1)
                ]

        elif border == 4:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] + 1), 6),
                    ((tile[0], tile[1] + 1), 3)
                ]
            else:
                return [
                    ((tile[0], tile[1] + 1), 6),
                    ((tile[0] + 1, tile[1] + 1), 3)
                ]

        elif border == 5:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1]), 3),
                    ((tile[0] - 1, tile[1] + 1), 1)
                ]
            else:
                return [
                    ((tile[0] - 1, tile[1]), 3),
                    ((tile[0], tile[1] + 1), 1)
                ]

        elif border == 6:
            if tile[1] % 2 == 0:
                return [
                    ((tile[0] - 1, tile[1] + 1), 4),
                    ((tile[0] - 1, tile[1]), 2)
                ]
            else:
                return [
                    ((tile[0], tile[1] + 1), 4),
                    ((tile[0] - 1, tile[1]), 2)
                ]

    # returns entire city dictionary or None
    @classmethod
    def getCity(cls, map, tile, border):
        siblings = cls.getSiblings(map, tile, border)

        for city in map.cities:
            if tile[0] == city['x'] and tile[1] == city['y'] and border == city['border']:
                return city

            if siblings[0][0][0] == city['x'] and tile[0][0][1] == city['y'] and siblings[0][1] == city['border']:
                return city

            if siblings[1][0][0] == city['x'] and siblings[1][0][1] == city['y'] and siblings[1][1] == city['border']:
                return city

        return None

    @classmethod
    def isOccupied(cls, map, tile, border):
        city = cls.getCity(map, tile, border)

        if city is None:
            return False
        return True