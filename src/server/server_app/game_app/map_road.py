"""
Borders:
     1    2
       /\    
    6 |  | 3
       \/
     5    4  

Tiles:
     0   2   3   4

0:   1   2   3   4
1:     5   6   7   8
2:   9   10  11  12
3:     13  14  15  16

"""

class MapRoad:
    
    """
        Considers siblings
        tile[0] - X axis
        tile[1] - Y axis
        border  - 1-6
        color   - 1-8
    """
    @classmethod 
    def canBeAdded(cls, map, tile, border, color):
        if border <= 0 or border > 6:
            return False

        if color <= 0 or color > 8:
            return False

        sibling = cls.getSibling(map, tile, border)
        if tile not in map.active_tiles and sibling[0] not in map.active_tiles:
            return False

        if cls.isOccupied(map, tile, border):
            return False

        if len(cls.getAdjacentRoads(map, tile, border, color)) == 0:
            return False

        return True

    """
        returns list of existing adjacent roads in the same color
    """
    @classmethod
    def getAdjacentRoads(cls, map, tile, border, color):
        pre_list = []
        sibling = cls.getSibling(map, tile, border)

        if border == 1:
            pre_list = [
                (tile, 2), (tile, 6),
                (sibling[0], 3), (sibling[0], 5)
            ]

        elif border == 2:
            pre_list = [
                (tile, 1), (tile, 3),
                (sibling[0], 6), (sibling[0], 4)
            ]

        elif border == 3:
            pre_list = [
                (tile, 2), (tile, 4),
                (sibling[0], 1), (sibling[0], 5)
            ]

        elif border == 4:
            pre_list = [
                (tile, 3), (tile, 5),
                (sibling[0], 2), (sibling[0], 6)
            ]

        elif border == 5:
            pre_list = [
                (tile, 4), (tile, 6),
                (sibling[0], 1), (sibling[0], 3)
            ]

        elif border == 6:
            pre_list = [
                (tile, 1), (tile, 5),
                (sibling[0], 2), (sibling[0], 4)
            ]

        final_list = []

        for elem in pre_list:
            if cls.getRoad(map, elem[0], elem[1]) == color:
                final_list.append(elem)

        return final_list


    """
        Borders:
            1    2
              /\    
           6 |  | 3
              \/
            5    4  
    """
    @classmethod
    def getSibling(cls, map, tile, border):
        if border == 1:
            if tile[1] % 2 == 0:
                return [[tile[0]-1, tile[1]-1], 4]
            else:
                return [[tile[0], tile[1]-1], 4]

        elif border == 2:
            if tile[1] % 2 == 0:
                return [[tile[0], tile[1]-1], 5]
            else:
                return [[tile[0]+1, tile[1]-1], 5]

        elif border == 3:
            return [[tile[0]+1, tile[1]], 6]

        elif border == 4:
            if tile[1] % 2 == 0:
                return [[tile[0], tile[1]+1], 1]
            else:
                return [[tile[0]+1, tile[1]+1], 1]

        elif border == 5:
            if tile[1] % 2 == 0:
                return [[tile[0]-1, tile[1]+1], 2]
            else:
                return [[tile[0], tile[1]+1], 2]

        elif border == 6:
            return [[tile[0]-1, tile[1]], 3]

        else:
            return None

    @classmethod
    def isOccupied(cls, map, tile, border):
        sibling = cls.getSibling(map, tile, border)

        for road in map.roads:
            if tile[0] == road['x'] and tile[1] == road['y'] and border == road['border']:
                return True

            if sibling[0][0] == road['x'] and sibling[0][1] == road['y'] and sibling[1] == road['border']:
                return True

        return False

    @classmethod
    def addRoad(cls, map, tile, border, color):
        map.roads.append({
            'x': tile[0],
            'y': tile[1],
            'border': border,
            'color': color
        })

    # returns color or None
    @classmethod
    def getRoad(cls, map, tile, border):
        sibling = cls.getSibling(map, tile, border)

        for road in map.roads:
            if tile[0] == road['x'] and tile[1] == road['y'] and border == road['border']:
                return road['color']

            if sibling[0][0] == road['x'] and sibling[0][1] == road['y'] and sibling[1] == road['border']:
                return road['color']

        return None