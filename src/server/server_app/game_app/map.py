import copy
import json

from .map_city import MapCity 
from .map_road import MapRoad 
from .map_settlement import MapSettlement 

class TileType:
    Hills = 1
    Forest = 2
    Mountains = 3
    Fields = 4
    Pasture = 5
    Desert = 6

    Water = 7
    
    Default = 7

class Map:
    def __init__(self, game_handle, W=12, H=8):
        self.w = W
        self.h = H

        self.active_tiles = []
        self.settlements = []
        self.cities = []
        self.roads = []

        self.robber = {
            'x': 0,
            'y': 0,
            'color': 0
        }
        self.sw = 0

        self.game = game_handle

    def isNewRoadValid(self, tile, border, color):
        return MapRoad.canBeAdded(self, tile, border, color)

    def addRoad(self, tile, border, color):
        if MapRoad.canBeAdded(self, tile, border, color):
            MapRoad.addRoad(self, tile, border, color)
            return True
        return False

    def addSettlement(self, tile, border, color):
        if MapSettlement.canBeAdded(self, tile, border, color):
            MapSettlement.addSettlement(self, tile, border, color)
            return True
        return False

    def addCity(self, tile, border, color):
        if MapCity.canBeAdded(self, tile, border, color):
            MapCity.addCity(self, tile, border, color)
            return True
        return False

    def setRobber(self, tile, color):
        if tile not in self.active_tiles:
            return False

        self.changeThief(tile, color)
        return True

    def changeThief(self, tile, color):
        self.robber['x'] = tile[0]
        self.robber['y'] = tile[1]
        self.robber['color'] = color

    # returns [color, length]
    def getLongestRoad(self):
        ret = [0, 0]

        # match roads to each player
        
        # iterate players
            # walk recursively through each road
            # calculate longest possible distance

        return ret

    def toSerializable(self):
        return {
            'width': self.w,
            'height': self.h,
            'active_tiles': self.active_tiles,
            'settlements': self.settlements,
            'cities': self.cities,
            'roads': self.roads,
            'robber': self.robber
        }

    def toJSON(self):
        return json.dumps({
            'width': self.w,
            'height': self.h,
            'active_tiles': self.active_tiles,
            'settlements': self.settlements,
            'cities': self.cities,
            'roads': self.roads,
            'robber': self.robber
        })

    def fromJSON(self, string):
        js = json.loads(string)
        self.w = js['width']
        self.h = js['height']
        self.active_tiles = js['active_tiles']
        self.settlements = js['settlements']
        self.cities = js['cities']
        self.roads = js['roads']
        self.robber = js['robber']
