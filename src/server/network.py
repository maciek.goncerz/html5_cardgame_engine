import json
import asyncio
import websockets
import functools

from server_app import CatanServer
from codes import Code

async def connect(websocket, path, server):
    async for message in websocket:
        await server.connectPlayer(websocket)

        try:
            await websocket.send('C')

            async for message in websocket:
                m = json.loads(message)

                # Login
                if m['code'] == Code.Login:
                    if await server.login(websocket, m['login'], m['hash']) == False:
                        await websocket.send('I')
                    else:
                        await websocket.send('L;{}'.format(server.players[websocket].name))
                        await server.updateUserStatus(websocket)

                # Adding new player
                elif m['code'] == Code.NewPlayer:
                    if await server.addNewPlayer(m['login'], m['hash']) == False:
                        await websocket.send('T')
                    else:
                        await websocket.send('A')

                # Server action
                elif m['code'] == Code.Action:
                    await server.action(websocket, m)

                # Starting new game
                elif m['code'] == Code.NewGame:
                    await server.newGame(websocket, m)

                # Join existing game
                elif m['code'] == Code.JoinGame:
                    if await server.joinGame(websocket, m) == False:
                        await websocket.send('R')
                    else:
                        await websocket.send('J')

                # Removing existing game (only by admin)
                elif m['code'] == Code.RemoveGame:
                    await server.removeGame(websocket, m)

                # Remove player's account and its information
                elif m['code'] == Code.DeletePlayer:
                    pass
                
                # When toggling between games to keep things most up-to-date
                elif m['code'] == Code.AcquireGameInfo:
                    if 'id' in m:
                        await server.updatePlayer(websocket, m['id'])

        finally:
            await server.disconnectPlayer(websocket)

def startServer(ip='0.0.0.0', port=54333):
    server = CatanServer()
    connect_function = functools.partial(connect, server=server)

    asyncio.get_event_loop().run_until_complete(
        websockets.serve(connect_function, "0.0.0.0", 80)
    )

    asyncio.get_event_loop().run_forever()