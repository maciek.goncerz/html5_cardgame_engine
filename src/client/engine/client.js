SocketClient = {
    sock: null,
    restore: -1,
    status: Status.Disconnected,
    connect: connectWithServer,
    
    /* CHECK IF CONNECTION IS ESTABLISHED -> IF NOT: RECONNECT */
    checkConnection: function() {
        if (SocketClient.status == Status.Disconnected ) {
            SocketClient.status = Status.Reconnecting;
            SocketClient.connect();
        }      
    },

    /* REGISTER */
    register: function(name, pin) {
        console.log('Registering new player: ' + name + ' (' + pin + ')');

        SocketClient.sock.send(JSON.stringify({
            'code': Code.NewPlayer,
            'login': name,
            'hash': CryptoJS.MD5(name + pin).toString()
        }));
    },

    /* LOGIN */
    login: function() {    
        if ( Cookies.get('login') == undefined || Cookies.get('pin') == undefined ) {
            console.log('Missing login cookies');
            return;
        }

        console.log('Sending login credentials: ' 
            + Cookies.get('login') + ' : ' 
            + CryptoJS.MD5(Cookies.get('login') + Cookies.get('pin')).toString()
        );

        SocketClient.sock.send(JSON.stringify({
            'code': Code.Login,
            'login': Cookies.get('login'),
            'hash': CryptoJS.MD5(Cookies.get('login') + Cookies.get('pin')).toString()
        }));
    },

    /* RELOG */
    relog: function() {
        if( SocketClient.status >= Status.Connected ) {
            console.log('Closing old connection for relog');
            SocketClient.sock.close(1000);
        }
    
        console.log('Starting new connection for relog')
        SocketClient.connect();
    },

    /* NEW GAME */
    newGame: function() {
        SocketClient.sock.send(JSON.stringify({
            'code': Code.NewGame
        }));
    },

    /* JOIN GAME */
    joinGame: function(game_id) {
        SocketClient.sock.send(JSON.stringify({
            'code': Code.JoinGame,
            'game': game_id
        }));
    },

    /* REMOVE GAME */
    removeGame: function(game_id) {
        SocketClient.sock.send(JSON.stringify({
            'code': Code.RemoveGame,
            'id': game_id
        }));
    },

    /* REQUEST GAME STATUS UPDATE FROM SERVER */
    updateGameStatus: function(game_id=null) {
        if (game_id == null)
            if (Client.chosen_game != null)
                game_id = Client.chosen_game;
            else
                return;

        SocketClient.sock.send(JSON.stringify({
            'code': Code.AcquireGameStatus,
            'id': game_id
        }));
    },

    /* PERFORM GAME ACTION */
    // args should be given as dictionary
    action: function(action, args=null, game_id=null) {
        if (game_id == null)
            if (Client.chosen_game != null)
                game_id = Client.chosen_game;
            else
                return;

        action_object = {
            'code': Code.Action,
            'game': game_id,
            'action': action
        }

        for (argument in args) {
            action_object[argument] = args[argument];
        }

        console.log('Sending: ' + JSON.stringify(action_object));
        SocketClient.sock.send(JSON.stringify(action_object));
    }
}

// currently not used
function updateStatus() {
    switch(SocketClient.status) {
        case Status.Disconnected:
            break;

        case Status.Connected:
            break;

        case Status.LoggedOut:
            break;

        case Status.LoggedIn:
        case Status.Waiting:
        case Status.Idle:
        case Status.Turn:
            break;
    }
}

function connectWithServer() {
    SocketClient.sock = new WebSocket("ws://localhost:54333");

    SocketClient.sock.onopen = onOpenEvent;
    SocketClient.sock.onmessage = onMessageEvent;
    SocketClient.sock.onclose = onCloseEvent;
    SocketClient.sock.onerror = onErrorEvent;
}

function onOpenEvent(e) {
    if (SocketClient.restore >= 0) {
        clearInterval(SocketClient.restore);
        SocketClient.restore = -1;
    }

    SocketClient.sock.send("Hello!");

    SocketClient.status = Status.Connected;
    updateStatus();
}

function onMessageEvent(e) {
    console.log(e.data);

    switch (e.data[0]) {
        case Response.Connected:
            SocketClient.login();
            break;

        case Response.LoggedIn:
            SocketClient.status = Status.LoggedIn;
            break;

        case Response.InvalidLogin:
            console.log("Nie udało się zalogować z danymi: " + Cookies.get('login') + " PIN: " + Cookies.get('pin'));
            SocketClient.status = Status.LoggedOut;
            break;

        case Response.UserAdded:
            // TODO: handle new user added
            break;

        case Response.PlayerStatus:
            // TODO: apply new player status
            break;

        case Response.GameStatus:
            let stat = JSON.parse(e.data.split(';')[1])

            if ('game_status' in stat) { // check if this is a game status update
                if ('game' in stat['game_status']) { // check if it has game component
                    if (stat['game_status']['game']['id'] == Client.chosen_game) { // check if it's neccesary
                        Client.game = stat['game_status']['game'];
                    }
                }
                if ('player' in stat['game_status']) { // check if it has player component
                    if (stat['game_status']['game']['id'] == Client.chosen_game) { // check if it's neccesary
                        Client.player = stat['game_status']['player'];
                    }
                }
            }

            if ('user_status' in stat) {
                Client.user = stat['user_status'];

                if ('games' in stat['user_status']) {
                    applyUserStatus(stat);
                }
            }

            console.log('Received status');
            console.log(stat);
            break;

        default:
            break;
    }

    updateStatus();
}

function onCloseEvent(e) {
    // SocketClient.restore = setInterval(connectWithServer, 1000);
    SocketClient.status = Status.Disconnected;
    updateStatus();
}

function onErrorEvent(e) {
    // TODO: implement some error handling
    SocketClient.status = Status.Disconnected;
}

function setLoginCredentials(login, pin) {
    Cookies.set('login', login);
    Cookies.set('pin', pin);
}

function lobbyAction(game_id, action_name, argument=null) {

}