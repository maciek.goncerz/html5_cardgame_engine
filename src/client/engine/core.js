// connect with server to initiate game
SocketClient.connect();

// start main program loop at 10 frames per second
setInterval(loop, 1000 / 15);

// init events
$('#game_plane').click(mouseClickEvent);
ctx.canvas.addEventListener('mousemove', onMouseMove);
ctx.canvas.addEventListener('contextmenu', function(){Client.hand = null;});

// resize event
matchCanvasSize();
window.onresize = onCanvasResize;

// main loop
function loop() {
    // game pauses until user relogs or restores connection
    if (SocketClient.status < Status.LoggedIn) {
        ctx.fillStyle = Global.logged_out;
        ctx.fillRect(0, 0, Res.width, Res.height);

        ensureMenuType('Logging');
        return;
    }

    // to ensure proper size calculations
    Res.width = ctx.canvas.width;
    Res.height = ctx.canvas.height;

    ctx.save();

    // fill backgroundd
    ctx.fillStyle = Global.logged_in;
    ctx.fillRect(0, 0, Res.width, Res.height);

    // render current game stage representation
    if (Client.game != null && Client.game['id'] == Client.chosen_game)
    {
        switch(Client.game.stage)
        {
            case GameStage.Lobby:
                ensureMenuType('Lobby');
                renderLobby(ctx);
                break;

            case GameStage.InGame:
                ensureMenuType('InGame');
                break;

            case GameStage.Finished:
                ensureMenuType('Finished');
                break;

            default:
                break;
        }
    } else {
        ensureMenuType('Default');
    }

    // draw entities on map
    // RENDER: cities, villages, paths, thief

    // apply chosen window
    // cards has a priority over players_list
    /*
    if (client_status.view['cards']) {
        // RENDER: cards
    } else if (client_status.view['players_list']) {
        // RENDER: players
    }
    */

    ctx.restore();
}

function matchCanvasSize() {
    elem = document.getElementsByClassName('canvas')[0];

    Res.width = elem.clientWidth;
    Res.height = elem.clientHeight;

    contex_element.width = elem.clientWidth;
    contex_element.height = elem.clientHeight;
}

function onCanvasResize() {
    matchCanvasSize();

    console.log('canvas resized')
}

function onMouseMove(event) {
    let mouse_pos = relMousePos(this, event);

    Client.cursor.x = mouse_pos.x;
    Client.cursor.y = mouse_pos.y;
}

function relMousePos(object, event) {
    let totalOffsetX = 0;
    let totalOffsetY = 0;
    let canvasX = 0;
    let canvasY = 0;
    let currentElement = object;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent)

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    return {x:canvasX, y:canvasY}
}

function distance(x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

function mouseClickEvent(event) {
    mouse_pos = relMousePos(this, event);

    // execute clickable functions
    for (ca in CA) {
        if (CA[ca].x1 == null) {
            continue;
        } else {
            if (mouse_pos.x < CA[ca].x1 || mouse_pos.x > CA[ca].x2)
                continue;

            if (mouse_pos.y < CA[ca].y1 || mouse_pos.y > CA[ca].y2)
                continue;

            CA[ca].response();
        }
    }

    if (Client.game != null) {
        if (Client.game.stage == GameStage.Lobby) {
            lobby_onClickEvent();
        } else if (Client.game.stage == GameStage.InGame) {

        } else if (Client.game.stage == GameStage.Finished) {

        }
    }
}

function setResolution(x, y) {
    cc.canvas.width = x;
    cc.canvas.height = y;

    Res.width = x;
    Res.height = y;

    let cols = map_definition['width'];
    let rows = map_definition['height'];
    
    let map_width = 1.5 * cols * units;
    let map_height = (2 * rows + 1) * 0.5 * units * Math.sqrt(3);

    // scale by map's height
    if ( Res.width / Res.height > map_width / map_height ) {
        scale = Res.height / 
            (textures_container[TileType.Default].texture.width * 0.5 * map_height);
        
    } else {
        scale = Res.width / 
            (textures_container[TileType.Default].texture.width * 0.5 * map_width);
        
        console.log(map_width);
    }
}