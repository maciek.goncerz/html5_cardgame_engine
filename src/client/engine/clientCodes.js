class Code {
    static Login = 1;
    static NewPlayer = 2; 
    static Action = 3;
    static NewGame = 4;
    static JoinGame = 8;
    static RemoveGame = 5;
    static DeletePlayer = 6;
    
    static AcquireGameStatus = 7;
}

class Response {
    static Connected = 'C';

    static InvalidLogin = 'I';
    static LoggedIn = 'L';
    static UserAdded = 'A';
    static NameTaken = 'T';

    static PlayerStatus = 'P';
    static GameStatus = 'S';

    static JoinedGame = 'J';
    static CannotJoin = 'R';
}

class Status {
    static Disconnected = 0;
    static Reconnecting = 0.5;
    static Connected = 1;
    static LoggedOut = 2;
    static LoggedIn = 3;
    static Waiting = 4;
    static Idle = 5;
    static Turn = 6;
}