CA = {
    test_area: {
        x1: null,
        y1: null,
        x2: null,
        y2: null,
        response: function() {console.log('Clicked');}
    }
}

function linkClickableArea(ca, rect) {
    ca.x1 = rect.x;
    ca.y1 = rect.y;
    ca.x2 = rect.x + rect.w;
    ca.y2 = rect.y + rect.h;
}

function unlinkAllClickableAreas() {
    for (ca in CA) {
        CA[ca].x1 = null;
        CA[ca].y1 = null;
        CA[ca].x2 = null;
        CA[ca].y2 = null;
    }
}