
class Color {
    static Red = 1;
    static Green = 2;
    static Blue = 3;
    static Black = 4;
    static Orange = 5;
    static Purple = 6;
    static White = 7;
    static Yellow = 8;

    static Default = 0;

    static hex = {
        0: '#c5c7c4',
        1: '#f00',
        2: '#0f0',
        3: '#00f',
        4: '#000',
        5: 'orange',
        6: 'purple',
        7: '#fff',
        8: '#ff0'
    };
}

function renderLobby(c) {
    c.save();

    /*
        Each lobby view is centered. (sort of)
    */

    // Render lobby view
    switch (Client.lobby_card)
    {
        // Players' view
        case 0:
            // players' slots rect approx.: 340 x 350
            // upper bar takes max. 70px
            // available workspace is then
            let h = 0.9 * (Res.height - 70);
            let w = 0.9 * Res.width;

            let scale = 1.0;

            // determine axis for scaling
            if (w / h > 340.0 / 350.0) {
                // scale to fit Y axis
                scale = h / 350.0;
            } else {
                scale = w / 340.0;
            }

            c.translate(Res.width / 2.0 - (scale * 170.0), 90);
            c.scale(scale, scale);

            let card_slots = [
                {x: 0, y: 0, w: 100},
                {x: 120, y: 0, w: 100},
                {x: 240, y: 0, w: 100},
                {x: 0, y: 200, w: 100},
                {x: 120, y: 200, w: 100},
                {x: 240, y: 200, w: 100}
            ]
            let players_count = Object.keys(Client.game.players).length;
            let initial_count = players_count;
            
            for (player in Client.game.players) {
                let status = '';
                if (Client.game.admin == player) {
                    status = 'admin';
                }

                renderCard(c, {
                        name: player, 
                        stat: status, 
                        color: Client.game.players[player]
                    }, 
                    card_slots[initial_count - players_count].x, 
                    card_slots[initial_count - players_count].y, 
                    card_slots[initial_count - players_count].w
                );

                --players_count;
            }

            for (let i = initial_count; i < 6; ++i) {
                renderCard(c, {
                        name: '', 
                        stat: '', 
                        color: 0
                    }, 
                    card_slots[i].x, 
                    card_slots[i].y, 
                    card_slots[i].w
                );
            }

            break;

        // Map view
        case 1:
            c.fillStyle = 'black';

            // draw queue if available
            if (Client.game.lobby_stage > 0) {
                let cube_size = 20;
                let pos_y = 250;

                c.save();
                for (let i = 0; i < Client.game.queue.length; ++i) {
                    let color = Client.game.players[Client.game.queue[(i + Client.game.turn) % Client.game.queue.length]];

                    c.fillStyle = Color.hex[color];
                    c.fillRect(5, pos_y, cube_size, cube_size);

                    pos_y += cube_size;
                }
                c.restore();
            }

            // midpoint
            let mp = {x: c.canvas.width * 0.5, y: (c.canvas.height - 100.0) * 0.5 + 100.0};

            // draw map
            CatanMap.draw(c, mp);

            // draw presettings
            for (let player_name in Client.game.presettings) {
                if (Client.game.presettings[player_name].r1 != null) {
                    CatanMap.drawRoad(
                        c,
                        Client.game.presettings[player_name].r1.x,
                        Client.game.presettings[player_name].r1.y,
                        Client.game.presettings[player_name].r1.border,
                        Client.game.players[player_name]
                    );
                }

                if (Client.game.presettings[player_name].r2 != null) {
                    CatanMap.drawRoad(
                        c,
                        Client.game.presettings[player_name].r2.x,
                        Client.game.presettings[player_name].r2.y,
                        Client.game.presettings[player_name].r2.border,
                        Client.game.players[player_name]
                    );
                }

                if (Client.game.presettings[player_name].s1 != null) {
                    CatanMap.drawSettlement(
                        c,
                        Client.game.presettings[player_name].s1.x,
                        Client.game.presettings[player_name].s1.y,
                        Client.game.presettings[player_name].s1.border,
                        Client.game.players[player_name]
                    );
                }

                if (Client.game.presettings[player_name].s2 != null) {
                    CatanMap.drawSettlement(
                        c,
                        Client.game.presettings[player_name].s2.x,
                        Client.game.presettings[player_name].s2.y,
                        Client.game.presettings[player_name].s2.border,
                        Client.game.players[player_name]
                    );
                }
            }

            // render hand
            switch (Client.hand) {
                case HandItem.Road:
                    // check if in range of any possible location
                    let possible_road = CatanMap.getNearbyTile(2);
                    
                    // cannot clip to any position
                    if (possible_road == null) {
                        CatanMap.drawAbsoluteRoad(c, Client.cursor.x, Client.cursor.y, 2, Client.game.players[Client.user.name], '#FF2222');
                    }

                    // possible position for road
                    else {
                        let road_pos = CatanMap.getClosestRoad(possible_road);
                        let glow = null;

                        if (CatanMap.isRoadOccupied(road_pos))
                            glow = '#FF2222';
                        else
                            glow = '#22FF22';

                        CatanMap.drawRoad(c, road_pos.x, road_pos.y, road_pos.border, Client.game.players[Client.user.name], glow);
                    }
                    break;

                case HandItem.Settlement:
                    // check if in range of any possible location
                    let possible_settlement = CatanMap.getNearbyTile(3);
                    
                    // cannot clip to any position
                    if (possible_settlement == null) {
                        CatanMap.drawAbsoluteSettlement(c, Client.cursor.x, Client.cursor.y, 2, Client.game.players[Client.user.name], '#FF2222');
                    }

                    // possible position for road
                    else {
                        let settlement_pos = CatanMap.getClosestCity(possible_settlement);
                        let glow = null;

                        if (CatanMap.isCityOccupied(settlement_pos))
                            glow = '#FF2222';
                        else
                            glow = '#22FF22';

                        CatanMap.drawSettlement(c, settlement_pos.x, settlement_pos.y, settlement_pos.border, Client.game.players[Client.user.name], glow);
                    }
                    break;

                default:
                    break;
            }
            break;

        default:
            break;
    }

    c.restore();
}

function drawCircle(c, x, y, r) {
    c.beginPath();
    c.arc(x, y, r, 0, 2 * Math.PI, false);
    c.fill();
}

function getAbsoluteRect(c, x, y, w, h) {
    let transform_matrix = c.getTransform();

    let translate_x = transform_matrix.e;
    let translate_y = transform_matrix.f;
    let scale = transform_matrix.a;

    return {
        x: x * scale + translate_x, 
        y: y * scale + translate_y, 
        w: w * scale, 
        h: h * scale
    };
}

function renderCard(ctx, player, x, y, width, height=0) {
    ctx.save();
    ctx.translate(x, y);

    if (height > 0) {
        ctx.scale(width / 150.0, height / 250.0);
    } else {
        ctx.scale(width / 150.0, width / 150.0);
    }

    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, 150, 220);

    ctx.fillStyle = '#000';
    ctx.font = '24px Consolas';

    ctx.fillText(player.name, 9 + 13.2 * (10 - player.name.length) * 0.5, 155);
    ctx.fillText(player.stat, 9 + 13.2 * (10 - player.stat.length) * 0.5, 175);

    ctx.fillStyle = Color.hex[player.color];
    ctx.fillRect(25, 25, 100, 100);

    ctx.fillStyle = '#000';
    ctx.fillRect(10, 185, 130, 25);

    ctx.restore();
}

function lobby_onClickEvent() {
    if (Client.game.stage != GameStage.Lobby) return;

    // PLAYERS VIEW
    if (Client.game.lobby_card == 0) {

    }

    // MAP VIEW
    else {
        switch( Client.hand ) {
            case HandItem.Road:
                // get possible road location
                let prl = CatanMap.getNearbyTile(2);

                if (prl == null)
                    return;

                // get road position
                let rp = CatanMap.getClosestRoad(prl);

                if (CatanMap.isRoadOccupied(rp))
                    return;

                SocketClient.action('road', {'position': rp});
                break;

            case HandItem.Settlement:
                // get possible road location
                let psl = CatanMap.getNearbyTile(3);

                if (psl == null)
                    return;

                // get settlement position
                let sp = CatanMap.getClosestCity(psl);

                if (CatanMap.isCityOccupied(sp))
                    return;

                SocketClient.action('settlement', {'position': sp});
                break;

            default:
                break;
        }
    }
}