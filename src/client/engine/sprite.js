class Sprite {
    constructor(texture_path, x = 0.1, y = 0.1) {
        this.scale_x = x;
        this.scale_y = y;
        this.texture = new Image();
        this.texture.src = texture_path;
        
        console.log('Loaded texture: ' + texture_path)
    }

    setScale = function(x, y) {
        this.scale_x = x;
        this.scale_y = y;
    }

    draw = function(ctx, x, y, width=0, height=0) {
        if (width > 0 && height > 0) {
            ctx.save();
            ctx.translate(x, y);
            ctx.scale(width / this.texture.width, height / this.texture.height);
        }
        
        ctx.drawImage(
            this.texture,
            0, 0
        );
        
        if (width > 0 && height > 0) {
            ctx.restore();
        }
    }

    drawTile = function(c, x, y, w) {       
        c.save();

        c.translate(x, y - w / Math.sqrt(3));
        c.scale(w / this.texture.width, w / this.texture.width);

        c.drawImage(
            this.texture,
            0, 0
        );

        c.restore();
    }
}