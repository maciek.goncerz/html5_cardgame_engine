function applyUserStatus(status) {
    /*
        <div class='game-object' id='game-443-4234-5343'>443-4234-5343</div>
        [...]
        <div class='game-object' id='game-000-0000-0000' onclick='createNewGame()'>Utwórz nową</div>
    */
    let inner_html = ''
    for (let i = 0; i < status['user_status']['games'].length; ++i) {
        inner_html += "<div class='game-object' id='";
        inner_html += status['user_status']['games'][i];
        inner_html += "' onclick='PageEvents.menu_onGameChange(\"";
        inner_html += status['user_status']['games'][i];
        inner_html += "\")'>";
        inner_html += status['user_status']['games'][i];
        inner_html += "</div>\n";
    }
    document.getElementById('games-list-container').innerHTML = inner_html;
}

function toggleAdminCard() {
    if (Client.user.name == Client.game.admin) {
        document.getElementById('admin-card').style.display = 'block';
    } else {
        document.getElementById('admin-card').style.display = 'none';
    }
}

// main connection loop
window.setTimeout(setCheckInterval, 1000);

function setCheckInterval() {
    window.setInterval(SocketClient.checkConnection, 500);
}

// set initial settings for menu bar
document.getElementById('menu-login').style.display = 'block';
document.getElementById('menu-main').style.display = 'none';