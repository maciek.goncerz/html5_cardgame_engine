PageEvents = {
    /*
        MAIN MENU
    */
    menu_onLogin: function() {
        let l = document.getElementById('login').value;
        let p = document.getElementById('password').value;
    
        Cookies.set('login', l);
        Cookies.set('pin', p);
    
        SocketClient.relog();
    },

    menu_onRegister: function() {
        let l = document.getElementById('r-login').value;
        let p = document.getElementById('r-password').value;
    
        SocketClient.register(l, p);
    },

    menu_onJoin: function() {
        let id = document.getElementById('join-game-id').value;

        SocketClient.joinGame(id);
    },

    menu_onNewGame: function() {
        SocketClient.newGame()
    },

    menu_onLogout: function() {
        Cookies.set('login', '');
        Cookies.set('pin', '');
    
        SocketClient.relog();
    },

    menu_onGameChange: function(game_id) {
        Client.chosen_game = game_id;
        SocketClient.updateGameStatus(game_id);
    },

    /*
        LOBBY
    */
    lobby_onCardChange: function(card_num) {
        Client.lobby_card = card_num;
        // TODO: card change
    },

    lobby_onAction: function(control_code, argument=null) {
        switch (control_code) {
            // Choose/deselect color
            case 0:
                if (argument == null)
                    return;
    
                if (Client.game == null)
                    return;
    
                if (Client.lobby_chosen_color == argument) {
                    SocketClient.action('deselect_color');
                    return;
                }
    
                // check if someone else already has this color
                for (player in Client.game.players) {
                    if (argument == Client.game.players[player])
                        return;
                }
    
                // send request to claim color
                SocketClient.action('choose_color', {color: argument});
                break;
    
            // Accept/reject lobby
            case 1:
                if (Client.lobby_accepted)
                    SocketClient.action('reject');
                else
                    SocketClient.action('accept');
                break;
    
            // grab settlement
            case 2:
                Client.hand = HandItem.Settlement;
                break;
    
            // grab road
            case 3:
                Client.hand = HandItem.Road;
                break;
    
            // clear presettings
            case 7:
                SocketClient.action('clear_presettings');
                break;

            // Accept players (admin only)
            case 4:
                if (Client.user.name != Client.game.admin)
                    return;
    
                SocketClient.action('accept_players');
                break;
    
            // Kick player (admin only)
            case 5:
                if (argument == null)
                    return
    
                if (Client.user.name != Client.game.admin)
                    return;
                
                SocketClient.action('kick', {player: argument});
                break;
    
            // Start game (admin only)
            case 6:
                if (Client.user.name != Client.game.admin)
                    return;
                    
                SocketClient.action('start');
                break;
    
            default:
                break;
        }
    }
}