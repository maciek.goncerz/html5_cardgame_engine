// hide unused parts of menu bar at current game stage
// triggered in core.js

MenuElements = {
    Logging: ['menu-login'],
    Default: ['menu-main'],
    Lobby: ['menu-lobby'],
    InGame: [],
    Finished: []
}

function hideStuff(id) {
    let element = document.getElementById(id);
    if (element == null)
        console.log('null - ' + id);

    element.style.display = 'none';
}
function showStuff(id) {
    let element = document.getElementById(id);
    if (element == null)
        console.log('null - ' + id);

    element.style.display = 'block';
}

function hideAll(list=null) {
    if (list == null) {
        for (element in MenuElements) {
            hideAll(MenuElements[element]);
        }
    } else {
        for (let i = 0; i < list.length; ++i) {
            hideStuff(list[i]);
        }
    }
}

function showAll(list) {
    for (let i = 0; i < list.length; ++i) {
        showStuff(list[i]);
    }
}

_last_menu_type = '';

function ensureMenuType(type) {
    if (_last_menu_type == type)
        return;

    if (type in MenuElements) {
        hideAll();
        showAll(MenuElements[type]);
    }
}

function hideMenuAtLogging() {
    showAll(MenuElements.Logging);
    hideAll(MenuElements.Default);
    hideAll(MenuElements.Lobby);
    hideAll(MenuElements.InGame);
    hideAll(MenuElements.Finished);
}

function hideMenuAtDefault() {
    hideAll(MenuElements.Logging);
    showAll(MenuElements.Default);
    hideAll(MenuElements.Lobby);
    hideAll(MenuElements.InGame);
    hideAll(MenuElements.Finished);
}
  
function hideMenuAtLobby() {
    hideAll(MenuElements.Logging);
    showAll(MenuElements.Default);
    showAll(MenuElements.Lobby);
    hideAll(MenuElements.InGame);
    hideAll(MenuElements.Finished);
}

function hideMenuAtInGame() {
    hideAll(MenuElements.Logging);
    showAll(MenuElements.Default);
    hideAll(MenuElements.Lobby);
    showAll(MenuElements.InGame);
    hideAll(MenuElements.Finished);
}
   
function hideMenuAtFinished() {
    hideAll(MenuElements.Logging);
    showAll(MenuElements.Default);
    hideAll(MenuElements.Lobby);
    hideAll(MenuElements.InGame);
    showAll(MenuElements.Finished);
}
