class Player {
    constructor(login, pin, color='#0F0') {
        this.login = login;
        this.hash = CryptoJS.MD5(login + pin);

        this.color = color;
        this.paths = new Array();
        this.entities = new Array();
    }
}

// defines current players container
this_player = Cookies.get('login');
current_players = {}