// map consists of hexagonal blocks

/*
    Tile borders:
        1    2
          /\    
       6 |  | 3
          \/
        5    4  
*/
const R = 1.0;                      // tile radius from center to any vertex
const L = R * 0.5 * Math.sqrt(3);   // tile length from center to any wall midpoint
                                    // thus, tile_width  = 2 * L
                                    // and   tile_height = 2 * R
class TileType {
    static Forest = 1;
    static Field = 2;
    static Crops = 3;
    static Rocks = 4;
    static Clay = 5;
    static Water = 6;

    static Default = 6;
}

CatanMap = {
    tile_radius: -1,
    tile_border: -1,
    ul_tile: {x: -1, y: -1},    // upper left most tile midpoint
    hs: -1,                     // horizontal step
    vs: -1,                     // vertical step

    draw: function(context, midpoint) {
        context.save();

        // estimate tile width
        let tile_width = CatanMap.maxTileWidth(context);

        // calculate basic descriptors of a single tile
        CatanMap.tile_border = 0.5 * tile_width;
        CatanMap.tile_radius = CatanMap.tile_border / L;

        CatanMap.hs = tile_width;     
        CatanMap.vs = L * tile_width; 

        // each map has a rectangular shape
        CatanMap.ul_tile = { 
            x: midpoint.x - Client.game.map.width * .5 * CatanMap.hs + CatanMap.tile_border,
            y: midpoint.y - Client.game.map.height * .5 * CatanMap.vs + CatanMap.tile_radius
        };

        // iterate through each tile in this map
        for (let x = 0; x < Client.game.map.width; ++x) {
            for (let y = 0; y < Client.game.map.height; ++y) {
                let tile_pos = CatanMap.getTilePos(x, y);

                textures_container[TileType.Clay].drawTile(
                    context,
                    tile_pos.x - CatanMap.tile_border,
                    tile_pos.y,
                    tile_width
                );
            }
        }

        context.restore();
    },

    // count - how many closest tiles should be returned
    getNearbyTile: function(count=1) {
        // determine start row
        let y_min = -1;
        for (let y = 0; y < Client.game.map.height; ++y) {
            if ( Client.cursor.y >= CatanMap.ul_tile.y + y * CatanMap.vs - CatanMap.tile_radius && 
                 Client.cursor.y <= CatanMap.ul_tile.y + y * CatanMap.vs + CatanMap.tile_radius ) {
                    y_min = y;
                    break;
                 }
            else {
                // console.log((CatanMap.ul_tile.y + 1 * CatanMap.vs - CatanMap.tile_radius) + ' - ' + Client.cursor.y + ' - ' + (CatanMap.ul_tile.y + 1 * CatanMap.vs + CatanMap.tile_radius));
            }
        }

        // mouse not over any row
        if (y_min < 0) return null;

        // find tiles that are in range of tile radius
        let tiles = []; // {x: -1, y: -1, d: 100000};
        for (let y of [y_min - 1, y_min, y_min + 1]) {
            if (y < 0 || y >= Client.game.map.height)
                continue;

            for (let x = 0; x < Client.game.map.width; ++x) {
                /*
                if (
                    Client.cursor.x >= CatanMap.ul_tile.x + x * CatanMap.hs + (y % 2) * .5 * CatanMap.hs - CatanMap.tile_border && 
                    Client.cursor.x <= CatanMap.ul_tile.x + x * CatanMap.hs + (y % 2) * .5 * CatanMap.hs + CatanMap.tile_border) {
                */

                // calculate absolute distance
                let d = distance(
                    Client.cursor.x, 
                    Client.cursor.y, 
                    CatanMap.ul_tile.x + x * CatanMap.hs + (y % 2) * .5 * CatanMap.hs, 
                    CatanMap.ul_tile.y + y * CatanMap.vs
                );

                tiles.push({x: x, y: y, d: d});
            }
        }

        // not enough points 
        if (tiles.length < count) return null;

        // sort tiles
        tiles.sort(function(a, b) {
            return a.d - b.d;
        });

        // mouse not over any tile
        if (tiles[0].d > CatanMap.tile_radius) return null;

        return tiles.slice(0, count);
    },

    // p1, p2, p3 are in tile numbers [0, Client.game.map.(width/height)]
    getClosestCity: function(p1, p2=null, p3=null) {
        if (p2 == null) { // if points are passed as an array
            p2 = p1[1];
            p3 = p1[2];
            p1 = p1[0];
        }

        let tile = p1;
        p1 = CatanMap.getTilePos(p1);
        p2 = CatanMap.getTilePos(p2);
        p3 = CatanMap.getTilePos(p3);

        let center = CatanMap.centroid(p1, p2, p3);
        let border = CatanMap.getCityFromVector(p1, center);

        return {x: tile.x, y: tile.y, border: border};
    },

    getClosestRoad: function(p1, p2=null) {
        if (p2 == null) {
            p2 = p1[1];
            p1 = p1[0];
        }

        let border = CatanMap.getBorderFromVector(
            CatanMap.getTilePos(p1),
            CatanMap.getTilePos(p2)
        );

        return {x: p1.x, y: p1.y, border: border};
    },

    getCityFromVector: function(p1, p2) {
        let angle = 180.0 * Math.atan(Math.abs(p2.y - p1.y) / Math.abs(p2.x - p1.x)) / Math.PI;

        // 6 1 2 (up)
        if (p2.y < p1.y) {
            if (angle > 60.0) {
                return 1;
            }

            else {
                if (p2.x < p1.x)
                    return 6;
                else
                    return 2;
            }
        }
        // 3 4 5 (down)
        else {
            if (angle > 60.0) {
                return 4;
            }
            else {
                if (p2.x < p1.x)
                    return 5;
                else
                    return 3;
            }
        }
    },

    getBorderFromVector: function(p1, p2) {
        // 1 5 6
        if (p2.x < p1.x) {
            // 1 6
            if (p2.y < p1.y) {
                if (Math.abs(p2.y - p1.y) / Math.abs(p2.x - p1.x) < CatanMap.tan30)
                    return 6;
                else
                    return 1;
            }
            // 5 6
            else {
                if (Math.abs(p2.y - p1.y) / Math.abs(p2.x - p1.x) < CatanMap.tan30)
                    return 6;
                else
                    return 5;
            }
        } 
        // 2 3 4
        else {
            // 2 3
            if (p2.y < p1.y) {
                if (Math.abs(p2.y - p1.y) / Math.abs(p2.x - p1.x) < CatanMap.tan30)
                    return 3;
                else
                    return 2;
            }
            // 3 4
            else {
                if (Math.abs(p2.y - p1.y) / Math.abs(p2.x - p1.x) < CatanMap.tan30)
                    return 3;
                else
                    return 4;
            }
        }
    },

    getTilePos: function(x, y=null) {
        if (y == null)
            return {
                x: CatanMap.ul_tile.x + x.x * CatanMap.hs + (x.y % 2) * .5 * CatanMap.hs,
                y: CatanMap.ul_tile.y + x.y * CatanMap.vs
            };

        return {
            x: CatanMap.ul_tile.x + x * CatanMap.hs + (y % 2) * .5 * CatanMap.hs,
            y: CatanMap.ul_tile.y + y * CatanMap.vs
        };
    },
    
    centroid: function(p1, p2, p3) {
        // calculate midpoint between p1 & p2
        let mp = {
            x: (p1.x + p2.x) * .5,
            y: (p1.y + p2.y) * .5
        };

        // calculate 2/3 distance from p3 -> mp
        return {
            x: p3.x + 2 * (mp.x - p3.x) / 3,
            y: p3.y + 2 * (mp.y - p3.y) / 3
        };
    },

    drawAbsoluteRoad: function(context, x, y, border=2, color=0, glow=null) {
        // calculate road dimensions
        let road_length = CatanMap.tile_radius * .8;
        let road_width = road_length * .3;
        
        let road_mp = {
            x: x,
            y: y
        };

        // draw road in proper orientation
        context.save();
        switch (border) {
            case 1:
            case 4:
                context.translate(
                    road_mp.x - CatanMap.cos150 * road_length * .5 + CatanMap.sin30 * road_width * .5,
                    road_mp.y - CatanMap.sin150 * road_length * .5 + CatanMap.cos30 * road_width * .5
                );
                context.rotate(150 / 180.0 * Math.PI);
                break;

            case 2:
            case 5:
                context.translate(
                    road_mp.x - CatanMap.cos30 * road_length * .5 + CatanMap.sin150 * road_width * .5, 
                    road_mp.y - CatanMap.sin30 * road_length * .5 + CatanMap.cos150 * road_width * .5
                );
                context.rotate(30 / 180.0 * Math.PI);
                break;

            case 3:
            case 6:
                context.translate(
                    road_mp.x - CatanMap.cos90 * road_length * .5 + CatanMap.sin90 * road_width * .5, 
                    road_mp.y - CatanMap.sin90 * road_length * .5 + CatanMap.cos90 * road_width * .5
                );
                context.rotate(90 / 180.0 * Math.PI);
                break;

            default:
                return;
        }

        // this method may vary depending on road drawing method
        if (glow != null) {
            context.fillStyle = glow;
            context.fillRect(-3, -3, road_length + 6, road_width + 6);
        }
        context.fillStyle = Color.hex[color];
        context.fillRect(0, 0, road_length, road_width);
        context.restore();
    },

    drawRoad: function(context, x, y, border, color=0, glow=null) {
        // acquire tile midpoint
        let tile_pos = CatanMap.getTilePos(x, y);

        // calculate road midpoint
        let road_mp = {
            x: tile_pos.x,
            y: tile_pos.y
        };

        // offset road midpoint to it's proper position
        switch (border) {
            case 1:
                road_mp.x -= CatanMap.tile_border * .5;
                road_mp.y -= CatanMap.tile_radius * .75;
                break;

            case 2:
                road_mp.x += CatanMap.tile_border * .5;
                road_mp.y -= CatanMap.tile_radius * .75;
                break;

            case 3:
                road_mp.x += CatanMap.tile_border;
                break;

            case 4:
                road_mp.x += CatanMap.tile_border * .5;
                road_mp.y += CatanMap.tile_radius * .75;
                break;

            case 5:
                road_mp.x -= CatanMap.tile_border * .5;
                road_mp.y += CatanMap.tile_radius * .75;
                break;

            case 6:
                road_mp.x -= CatanMap.tile_border;
                break;

            default:
                return;
        }

        // calculate road dimensions
        let road_length = CatanMap.tile_radius * .8;
        let road_width = road_length * .3;

        // draw road in proper orientation
        context.save();
        switch (border) {
            case 1:
            case 4:
                context.translate(
                    road_mp.x - CatanMap.cos150 * road_length * .5 + CatanMap.sin30 * road_width * .5,
                    road_mp.y - CatanMap.sin150 * road_length * .5 + CatanMap.cos30 * road_width * .5
                );
                context.rotate(150 / 180.0 * Math.PI);
                break;

            case 2:
            case 5:
                context.translate(
                    road_mp.x - CatanMap.cos30 * road_length * .5 + CatanMap.sin150 * road_width * .5, 
                    road_mp.y - CatanMap.sin30 * road_length * .5 + CatanMap.cos150 * road_width * .5
                );
                context.rotate(30 / 180.0 * Math.PI);
                break;

            case 3:
            case 6:
                context.translate(
                    road_mp.x - CatanMap.cos90 * road_length * .5 + CatanMap.sin90 * road_width * .5, 
                    road_mp.y - CatanMap.sin90 * road_length * .5 + CatanMap.cos90 * road_width * .5
                );
                context.rotate(90 / 180.0 * Math.PI);
                break;

            default:
                return;
        }

        // this method may vary depending on road drawing method
        if (glow != null) {
            context.fillStyle = glow;
            context.fillRect(-3, -3, road_length + 6, road_width + 6);
        }
        context.fillStyle = Color.hex[color];
        context.fillRect(0, 0, road_length, road_width);
        context.restore();
    },

    drawAbsoluteSettlement: function(context, x, y, border, color=0, glow=null) {
        // draw settlement, no need to orient it
        let settlement_width = .8 * CatanMap.tile_radius * .4;

        let settlement_mp = {
            x: x,
            y: y
        };

        context.save();
        if (glow != null) {
            context.fillStyle = glow;
            drawCircle(context, settlement_mp.x, settlement_mp.y, (settlement_width / 2.0) + 3);
        }
        context.fillStyle = Color.hex[color];
        drawCircle(context, settlement_mp.x, settlement_mp.y, settlement_width / 2.0);
        context.restore();
    },

    drawSettlement: function(context, x, y, border, color=0, glow=null) {
        // acquire tile midpoint
        let tile_pos = CatanMap.getTilePos(x, y);

        // calculate road midpoint
        let settlement_mp = {
            x: tile_pos.x,
            y: tile_pos.y
        };

        // offset road midpoint to it's proper position
        switch (border) {
            case 1:
                settlement_mp.y -= CatanMap.tile_radius;
                break;

            case 2:
                settlement_mp.x += CatanMap.tile_border;
                settlement_mp.y -= CatanMap.tile_radius * .5;
                break;

            case 3:
                settlement_mp.x += CatanMap.tile_border;
                settlement_mp.y += CatanMap.tile_radius * .5;
                break;

            case 4:
                settlement_mp.y += CatanMap.tile_radius;
                break;

            case 5:
                settlement_mp.x -= CatanMap.tile_border;
                settlement_mp.y += CatanMap.tile_radius * .5;
                break;

            case 6:
                settlement_mp.x -= CatanMap.tile_border;
                settlement_mp.y -= CatanMap.tile_radius * .5;
                break;

            default:
                return;
        }

        // draw settlement, no need to orient it
        let settlement_width = .8 * CatanMap.tile_radius * .4;

        context.save();
        if (glow != null) {
            context.fillStyle = glow;
            drawCircle(context, settlement_mp.x, settlement_mp.y, (settlement_width / 2.0) + 3);
        }
        context.fillStyle = Color.hex[color];
        drawCircle(context, settlement_mp.x, settlement_mp.y, settlement_width / 2.0);
        context.restore();
    },

    drawAbsoluteCity: function(context, x, y, border, color=0, glow=null) {
        // draw city, no need to orient it
        let city_width = CatanMap.tile_radius * .4;

        let city_mp = {
            x: x,
            y: y
        };

        context.save();
        if (glow != null) {
            context.fillStyle = glow;
            drawCircle(context, settlement_mp.x, settlement_mp.y, (city_width / 2.0) + 3);
        }
        context.fillStyle = Color.hex(color);
        drawCircle(context, city_mp.x, city_mp.y, city_width / 2.0);
        context.fillStyle = 'black';
        drawCircle(context, city_mp.x, city_mp.y, 0.6 * city_width / 2.0);
        context.restore();
    },

    drawCity: function(context, x, y, border, color=0, glow=null) {
        // acquire tile midpoint
        let tile_pos = CatanMap.getTilePos(x, y);

        // calculate road midpoint
        let city_mp = {
            x: tile_pos.x,
            y: tile_pos.y
        };

        // offset road midpoint to it's proper position
        switch (border) {
            case 1:
                city_mp.y -= CatanMap.tile_radius;
                break;

            case 2:
                city_mp.x += CatanMap.tile_border;
                city_mp.y -= CatanMap.tile_radius * .5;
                break;

            case 3:
                city_mp.x += CatanMap.tile_border;
                city_mp.y += CatanMap.tile_radius * .5;
                break;

            case 4:
                city_mp.y += CatanMap.tile_radius;
                break;

            case 5:
                city_mp.x -= CatanMap.tile_border;
                city_mp.y += CatanMap.tile_radius * .5;
                break;

            case 6:
                city_mp.x -= CatanMap.tile_border;
                city_mp.y -= CatanMap.tile_radius * .5;
                break;

            default:
                return;
        }

        // draw city, no need to orient it
        let city_width = CatanMap.tile_radius * .4;

        context.save();
        if (glow != null) {
            context.fillStyle = glow;
            drawCircle(context, settlement_mp.x, settlement_mp.y, (city_width / 2.0) + 3);
        }
        context.fillStyle = Color.hex(color);
        drawCircle(context, city_mp.x, city_mp.y, city_width / 2.0);
        context.fillStyle = 'black';
        drawCircle(context, city_mp.x, city_mp.y, 0.6 * city_width / 2.0);
        context.restore();
    },

    getRoadSibling: function(x, y, border) {
        switch(border) {
            case 1:
                return (y % 2 == 0) ?
                    {x: x-1, y: y-1, border: 4} :
                    {x: x, y: y-1, border: 4};
            
            case 2:
                return (y % 2 == 0) ?
                    {x: x, y: y-1, border: 5} :
                    {x: x+1, y: y-1, border: 5};

            case 3:
                return {x: x+1, y: y, border: 6};

            case 4:
                return (y % 2 == 0) ?
                    {x: x, y: y+1, border: 1} :
                    {x: x+1, y: y+1, border: 1};

            case 5:
                return (y % 2 == 0) ?
                    {x: x-1, y: y+1, border: 2} :
                    {x: x, y: y+1, border: 2};

            case 6:
                return {x: x-1, y: y, border: 3};
        }
    },

    isRoadOccupied: function(x, y=null, border=null) {
        if (y == null) {
            y = x.y;
            border = x.border;
            x = x.x;
        }

        let sibling = CatanMap.getRoadSibling(x, y, border);

        // check presettings
        if (Client.game.stage == GameStage.Lobby) {
            for (let player in Client.game.players) {
                if (Client.game.presettings[player].r1 != null) {
                    if (Client.game.presettings[player].r1.x == x && Client.game.presettings[player].r1.y == y && Client.game.presettings[player].r1.border == border) 
                        return true;
        
                    if (Client.game.presettings[player].r1.x == sibling.x && Client.game.presettings[player].r1.y == sibling.y && Client.game.presettings[player].r1.border == sibling.border) 
                        return true;
                }
                
                if (Client.game.presettings[player].r2 != null) {
                    if (Client.game.presettings[player].r2.x == x && Client.game.presettings[player].r2.y == y && Client.game.presettings[player].r2.border == border) 
                        return true;

                    if (Client.game.presettings[player].r2.x == sibling.x && Client.game.presettings[player].r2.y == sibling.y && Client.game.presettings[player].r2.border == sibling.border) 
                        return true;
                }
            }
        } else {
            for (let road of Client.game.map.roads) {
                if (road.x == x && road.y == y && road.border == border) 
                    return true;
    
                if (road.x == sibling.x && road.y == sibling.y && road.border == sibling.border) 
                    return true;
            }
        }

        return false;
    },

    getCitySiblings: function(x, y, border) {
        switch (border) {
            case 1:
                if (y % 2 == 0) {
                    return [
                        {x: x - 1, y: y - 1, border: 3},
                        {x: x, y: y - 1, border: 5}
                    ];
                } else {
                    return [
                        {x: x, y: y - 1, border: 3},
                        {x: x + 1, y: y - 1, border: 5}
                    ];
                }
            
            case 2:
                if (y % 2 == 0) {
                    return [
                        {x: x, y: y - 1, border: 4},
                        {x: x + 1, y: y, border: 6}
                    ];
                } else {
                    return [
                        {x: x + 1, y: y - 1, border: 4},
                        {x: x + 1, y: y, border: 6}
                    ];
                }
            
            case 3:
                if (y % 2 == 0) {
                    return [
                        {x: x + 1, y: y, border: 5},
                        {x: x, y: y + 1, border: 1}
                    ];
                } else {
                    return [
                        {x: x + 1, y: y, border: 5},
                        {x: x + 1, y: y + 1, border: 1}
                    ];
                }
            
            case 4:
                if (y % 2 == 0) {
                    return [
                        {x: x - 1, y: y + 1, border: 6},
                        {x: x, y: y + 1, border: 3}
                    ];
                } else {
                    return [
                        {x: x, y: y + 1, border: 6},
                        {x: x + 1, y: y + 1, border: 3}
                    ];
                }
            
            case 5:
                if (y % 2 == 0) {
                    return [
                        {x: x - 1, y: y, border: 3},
                        {x: x - 1, y: y + 1, border: 1}
                    ];
                } else {
                    return [
                        {x: x - 1, y: y, border: 3},
                        {x: x, y: y + 1, border: 1}
                    ];
                }
            
            case 6:
                if (y % 2 == 0) {
                    return [
                        {x: x - 1, y: y + 1, border: 4},
                        {x: x - 1, y: y, border: 2}
                    ];
                } else {
                    return [
                        {x: x, y: y + 1, border: 4},
                        {x: x - 1, y: y, border: 2}
                    ];
                }
        }
    },

    isCityOccupied: function(x, y=null, border=null) {
        if (y == null) {
            y = x.y;
            border = x.border;
            x = x.x;
        }

        let siblings = CatanMap.getCitySiblings(x, y, border);

        if (Client.game.stage == GameStage.Lobby) {
            for (let player in Client.game.players) {
                if (Client.game.presettings[player].s1 != null) {
                    if (Client.game.presettings[player].s1.x == x && Client.game.presettings[player].s1.y == y && Client.game.presettings[player].s1.border == border) 
                        return true;
        
                    if (Client.game.presettings[player].s1.x == siblings[0].x && Client.game.presettings[player].s1.y == siblings[0].y && Client.game.presettings[player].s1.border == siblings[0].border) 
                        return true;
                        
                    if (Client.game.presettings[player].s1.x == siblings[0].x && Client.game.presettings[player].s1.y == siblings[0].y && Client.game.presettings[player].s1.border == siblings[0].border) 
                        return true;
                }
                
                if (Client.game.presettings[player].s2 != null) {
                    if (Client.game.presettings[player].s2.x == x && Client.game.presettings[player].s2.y == y && Client.game.presettings[player].s2.border == border) 
                        return true;

                    if (Client.game.presettings[player].s2.x == siblings[1].x && Client.game.presettings[player].s2.y == siblings[1].y && Client.game.presettings[player].s2.border == siblings[1].border) 
                        return true;
                        
                    if (Client.game.presettings[player].s2.x == siblings[1].x && Client.game.presettings[player].s2.y == siblings[1].y && Client.game.presettings[player].s2.border == siblings[1].border) 
                        return true;
                }
            }
        } else {
            for (let settlement of Client.game.map.settlements) {
                if (settlement.x == x && settlement.y == y && settlement.border == border) 
                    return true;
    
                if (settlement.x == sibling[0].x && settlement.y == sibling[0].y && settlement.border == sibling[0].border) 
                    return true;
    
                if (settlement.x == sibling[1].x && settlement.y == sibling[1].y && settlement.border == sibling[1].border) 
                    return true;
            }
            
            for (let city of Client.game.map.cities) {
                if (city.x == x && city.y == y && city.border == border) 
                    return true;
    
                if (city.x == sibling[0].x && city.y == sibling[0].y && city.border == sibling[0].border) 
                    return true;
    
                if (city.x == sibling[1].x && city.y == sibling[1].y && city.border == sibling[1].border) 
                    return true;
            }
        }

        return false;
    },

    // some constant values to simplify unnecessary calculations
    sin30: Math.sin(30 / 180.0 * Math.PI),
    cos30: Math.cos(30 / 180.0 * Math.PI),
    sin150: Math.sin(150 / 180.0 * Math.PI),
    cos150: Math.cos(150 / 180.0 * Math.PI),
    sin90: Math.sin(90 / 180.0 * Math.PI),
    cos90: Math.cos(90 / 180.0 * Math.PI),
    tan30: Math.tan(30 / 180.0 * Math.PI),

    maxTileWidth: function(context) {
        // assumes that we want to render in rect:
        let render_rect = {
            x1: 0,
            y1: 50,
            x2: context.canvas.width,
            y2: context.canvas.height
        }

        // calculate maximum tile width & height
        let max_w = Math.abs(render_rect.x2 - render_rect.x1) / Client.game.map.width;
        let max_h = Math.abs(render_rect.y2 - render_rect.y1) / Client.game.map.height;
    
        return Math.min(
            max_w,
            max_h * L
        )
    }
}