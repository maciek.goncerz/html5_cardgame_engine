GameStage = {
    Lobby: 0,
    InGame: 1,
    Finished: 2
};

HandItem = {
    Road: 0,
    Settlement: 1,
    City: 2,
    Robber: 3
};

Client = {
    chosen_game: null,

    /* LOBBY */
    
    // 0 - players
    // 1 - map
    lobby_card: 0, 
    lobby_accepted: false,
    lobby_chosen_color: 0,

    // sends them only if all 4 are set
    lobby_settlement1: null,
    lobby_settlement2: null,
    lobby_road1: null,
    lobby_road2: null,

    /* IN-GAME */

    // TODO: everything

    /* GENERAL */

    /* 
        Player can hold sth in his hand
    */
    hand: null,

    /*
    Dictionary that has information on currently logged in user:
        - [string] 'name'      - player's name
        - [dict]   'games'     - ids' matched to game stages
    */
    user: null,

    /*
    Dictionary that matches game ID to current 
    information on it received from server.

    Consists of information on:
        - [dict] 'player' - player status
        - [int]    'points'            - victory points calculated for this player
        - [int]    'longest_road'      - current longest road count
        - [int]    'knights_count'     - number of used knights cards
        - [int]    'stage'             - game stage
        - [dict]   'resources'         - dictionary with resource information
        - [array]  'development_cards' - available development cards
        - [string] 'id'                - game id
        - [dict] 'game' - game status
        - [int]    'stage'             - game stage
        - [dict]   'players'           - players' names matched to their colors
        - [dict]   'special_cards'     - special cards' names matched to their owners
        - [dict]   'lobby'             - players' names matched to whether they have accepted lobby settings
        - [array]  'roll'              - array with 2 int's representing rolled dices
        - [array]  'queue'             - players' queue
        - [string] 'map'               - string JSON information of map
        - [string] 'admin'             - admin name
        - [string] 'current'           - current turn owner's name
        - [string] 'id'                - game id
    */
    player: null,
    game: null,

    // click areas
    cursor: {x: 0, y: 0},
    click_areas: []
}

client_status = {
    connection: {
        status: Status.Disconnected
    }
}