// this file loads textures

textures_container = {};

// load hexagonal tiles
textures_container[TileType.Forest] = new Sprite('./game/textures/lumber.png');
textures_container[TileType.Field]  = new Sprite('./game/textures/wool.png');
textures_container[TileType.Crops]  = new Sprite('./game/textures/crops.png');
textures_container[TileType.Rocks]  = new Sprite('./game/textures/ore.png');
textures_container[TileType.Clay]   = new Sprite('./game/textures/brick.png');
textures_container[TileType.Water]  = new Sprite('./game/textures/water.png');

Tile.setTileWidth(textures_container[TileType.Default].texture.width);
